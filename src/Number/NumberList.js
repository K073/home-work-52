import React from 'react';
import './Number.css';

const NumberList = (props) => {
    const listItem = props.numbers.map((number, index) =>
        <span className="Number-list-item" key={index}>{number}</span>
    );

    return (
        <div>{listItem}</div>
    );
};

export default NumberList;