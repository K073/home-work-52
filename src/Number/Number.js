import React, {Component} from 'react';
import './Number.css';
import NumberList from "./NumberList";

class Number extends Component{
    arrNumbers = [];
    arrDefault = [];

    generateNumbers = () => {
        this.arrDefault = [];
        this.arrNumbers = [];

        for (let i = 5; i <= 36; i++){
            this.arrDefault.push(i);
        }

        for (let i = 0; i < 5; i++){
            this.arrNumbers.push(this.arrDefault.splice(Math.floor(Math.random() * this.arrDefault.length), 1))
        }

        this.arrNumbers.sort((a, b) => a - b);
    };

    reRenderNumbers = () => {
        this.generateNumbers();
        this.setState(this.arrNumbers);
    };

    render () {
        this.generateNumbers();
        return (
            <div className="Number">
                <button className="btn" onClick={this.reRenderNumbers}>New number</button>
                <NumberList numbers={this.arrNumbers} />
            </div>
        );
    }
}

export default Number;